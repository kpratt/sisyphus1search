// the YQT group staff descriptions //////////////////////////////

researcher(Werner)//Werner
in-group(Werner, YQT)
in-project(Werner, RESPECT)
hacker(Werner)
works-with(Werner, Angi)
works-with(Werner, Marc)

researcher(Jurgen)
in-group(Jurgen, YQT)
in-project(Jurgen,EULISP)
hacker(Jurgen)
works-with(Jurgen,{Harry, Thomas})

researcher(Marc)
in-group(Marc, YQT)
in-project(Marc, KRIPTON)
hacker(Marc)
works-with(Marc,{Angi, Werner})

researcher(Angi)
in-group(Angi, YQT)
in-project(Angi, RESPECT)
works-with(Angi, {Marc, Werner})

researcher(Andy)
in-group(Andy, YQT)
in-project(Andy,TUTOR2000)
smoker(Andy)

researcher(Michael)
in-group(Michael, YQT)
in-project(Michael, "BABYLON Product")
hacker(Michael)
works-with(Michael,{Hans})

researcher(Harry)
in-group(Harry, YQT)
in-project(Harry, EULISP)
hacker(Harry)
works-with(Harry,{Jurgen, Thomas})

researcher(Uwe)
in-group(Uwe, YQT)
in-project(Uwe, "Autonomous Systems")
smoker(Uwe)
hacker(Uwe)

researcher(Thomas)
in-group(Thomas, YQT)
heads-group(Thomas, YQT)
in-project(Thomas, EULISP)
works-with(Thomas,{Jurgen, Harry})

secretary(Monika)
in-group(Monika, YQT)
works-with(Monika,{Thomas, Ulrike, Eva})

secretary(Ulrike)
in-group(Ulrike, YQT)
works-with(Ulrike,{Thomas, Monika, Eva})

researcher(Hans)
in-group(Hans, YQT)
in-project(Hans, "BABYLON Product")
heads-project(Hans, "BABYLON Product")
smoker(Hans)
works-with(Hans,{Michael})

manager(Eva)
in-group(Eva, YQT)
works-with(Eva,{Thomas, Ulrike, Monika})

researcher(Joachim)
in-group(Joachim, YQT)
in-project(Joachim, ASERTI)
heads-project(Joachim, ASERTI)

researcher(Katharina)
in-group(Katharina, YQT)
in-project(Katharina, MLT)
heads-project(Katharina, MLT)
smoker(Katharina)
hacker(Katharina)

// project descriptions
large-project("BABYLON Product")
large-project(ASERTI)
large-project(MLT)

// room sizes
small-room (C5113)
small-room (C5114)
small-room (C5115)
small-room (C5116)
large-room (C5117)
large-room (C5119)
large-room (C5120)
medium-room (C5121)
medium-room (C5122)
medium-room (C5123)

// room proximity
close(C5113, {C5114,C5115})
close(C5114, C5115)
close(C5114, C5116)
close(C5115, C5116)
close(C5115, C5117)
close(C5116, C5117)
close(C5117, C5119)
close(C5119, C5120)
close(C5119, C5121)
close(C5119, C5122)
close(C5120, C5121)
close(C5121, C5120)
close(C5121, C5122)
close(C5121, C5123)

// the TQY group staff descriptions ///////////////////////////////

researcher(Alice)//Alice
in-group(Alice, TQY)
in-project(Alice, Apple)
hacker(Alice)
works-with(Alice, Don)
works-with(Alice, Carol)

researcher(Bob)
in-group(Bob, TQY)
in-project(Bob,BCD)
hacker(Bob)
works-with(Bob,{George, Icabod})

researcher(Carol)
in-group(Carol, TQY)
in-project(Carol, CASA)
hacker(Carol)
works-with(Carol,{Don, Alice})

researcher(Don)
in-group(Don, TQY)
in-project(Don, Apple)
works-with(Don, {Carol, Alice})

researcher(Elizabeth)
in-group(Elizabeth, TQY)
in-project(Elizabeth,Diablo)
smoker(Elizabeth)

researcher(Fred)
in-group(Fred, TQY)
in-project(Fred, Elypse)
hacker(Fred)
works-with(Fred,{Len})

researcher(George)
in-group(George, TQY)
in-project(George, BCD)
hacker(George)
works-with(George,{Bob, Icabod})

researcher(Harold)
in-group(Harold, TQY)
in-project(Harold, FUBAR)
smoker(Harold)
hacker(Harold)

researcher(Icabod)
in-group(Icabod, TQY)
heads-group(Icabod, TQY)
in-project(Icabod, BCD)
works-with(Icabod,{Bob, George})

secretary(Jim)
in-group(Jim, TQY)
works-with(Jim,{Icabod, Karen, Marg})

secretary(Karen)
in-group(Karen, TQY)
works-with(Karen,{Icabod, Jim, Marg})

researcher(Len)
in-group(Len, TQY)
in-project(Len, Elypse)
heads-project(Len, Elypse)
smoker(Len)
works-with(Len,{Fred})

manager(Marg)
in-group(Marg, TQY)
works-with(Marg,{Icabod, Karen, Jim})

researcher(Nancy)
in-group(Nancy, TQY)
in-project(Nancy, Groupware)
heads-project(Nancy, Groupware)

researcher(Oley)
in-group(Oley, TQY)
in-project(Oley, Hardware)
heads-project(Oley, Hardware)
smoker(Oley)
hacker(Oley)

// project descriptions
large-project(Elypse)
large-project(Groupware)
large-project(Hardware)

// room sizes
small-room (C5213)
small-room (C5214)
small-room (C5215)
small-room (C5216)
large-room (C5217)
large-room (C5219)
large-room (C5220)
medium-room (C5221)
medium-room (C5222)
medium-room (C5223)

// room proximity
close(C5213, {C5214,C5215})
close(C5214, C5215)
close(C5214, C5216)
close(C5215, C5216)
close(C5215, C5217)
close(C5216, C5217)
close(C5217, C5219)
close(C5219, C5220)
close(C5219, C5221)
close(C5219, C5222)
close(C5220, C5221)
close(C5221, C5220)
close(C5221, C5222)
close(C5221, C5223)

// the YQT_ group staff descriptions //////////////////////////////

researcher(Werner_)//Werner_
in-group(Werner_, YQT_)
in-project(Werner_, RESPECT_)
hacker(Werner_)
works-with(Werner_, Angi_)
works-with(Werner_, Marc_)

researcher(Jurgen_)
in-group(Jurgen_, YQT_)
in-project(Jurgen_,EULISP_)
hacker(Jurgen_)
works-with(Jurgen_,{Harry_, Thomas_})

researcher(Marc_)
in-group(Marc_, YQT_)
in-project(Marc_, KRIPTON_)
hacker(Marc_)
works-with(Marc_,{Angi_, Werner_})

researcher(Angi_)
in-group(Angi_, YQT_)
in-project(Angi_, RESPECT_)
works-with(Angi_, {Marc_, Werner_})

researcher(Andy_)
in-group(Andy_, YQT_)
in-project(Andy_,TUTOR2000_)
smoker(Andy_)

researcher(Michael_)
in-group(Michael_, YQT_)
in-project(Michael_, "BABYLON Product_")
hacker(Michael_)
works-with(Michael_,{Hans_})

researcher(Harry_)
in-group(Harry_, YQT_)
in-project(Harry_, EULISP_)
hacker(Harry_)
works-with(Harry_,{Jurgen_, Thomas_})

researcher(Uwe_)
in-group(Uwe_, YQT_)
in-project(Uwe_, "Autonomous Systems")
smoker(Uwe_)
hacker(Uwe_)

researcher(Thomas_)
in-group(Thomas_, YQT_)
heads-group(Thomas_, YQT_)
in-project(Thomas_, EULISP_)
works-with(Thomas_,{Jurgen_, Harry_})

secretary(Monika_)
in-group(Monika_, YQT_)
works-with(Monika_,{Thomas_, Ulrike_, Eva_})

secretary(Ulrike_)
in-group(Ulrike_, YQT_)
works-with(Ulrike_,{Thomas_, Monika_, Eva_})

researcher(Hans_)
in-group(Hans_, YQT_)
in-project(Hans_, "BABYLON Product_")
heads-project(Hans_, "BABYLON Product_")
smoker(Hans_)
works-with(Hans_,{Michael_})

manager(Eva_)
in-group(Eva_, YQT_)
works-with(Eva_,{Thomas_, Ulrike_, Monika_})

researcher(Joachim_)
in-group(Joachim_, YQT_)
in-project(Joachim_, ASERTI_)
heads-project(Joachim_, ASERTI_)

researcher(Katharina_)
in-group(Katharina_, YQT_)
in-project(Katharina_, MLT_)
heads-project(Katharina_, MLT_)
smoker(Katharina_)
hacker(Katharina_)

// project descriptions
large-project("BABYLON Product_")
large-project(ASERTI_)
large-project(MLT_)

// room sizes
small-room (C5313)
small-room (C5314)
small-room (C5315)
small-room (C5316)
large-room (C5317)
large-room (C5319)
large-room (C5320)
medium-room (C5321)
medium-room (C5322)
medium-room (C5323)

// room proximity
close(C5313, {C5314,C5315})
close(C5314, C5315)
close(C5314, C5316)
close(C5315, C5316)
close(C5315, C5317)
close(C5316, C5317)
close(C5317, C5319)
close(C5319, C5320)
close(C5319, C5321)
close(C5319, C5322)
close(C5320, C5321)
close(C5321, C5320)
close(C5321, C5322)
close(C5321, C5323)


// the TQY_ group staff descriptions ///////////////////////////////

researcher(Alice_)//Alice_
in-group(Alice_, TQY_)
in-project(Alice_, Apple_)
hacker(Alice_)
works-with(Alice_, Don_)
works-with(Alice_, Carol_)

researcher(Bob_)
in-group(Bob_, TQY_)
in-project(Bob_,BCD_)
hacker(Bob_)
works-with(Bob_,{George_, Icabod_})

researcher(Carol_)
in-group(Carol_, TQY_)
in-project(Carol_, CASA_)
hacker(Carol_)
works-with(Carol_,{Don_, Alice_})

researcher(Don_)
in-group(Don_, TQY_)
in-project(Don_, Apple_)
works-with(Don_, {Carol_, Alice_})

researcher(Elizabeth_)
in-group(Elizabeth_, TQY_)
in-project(Elizabeth_,Diablo_)
smoker(Elizabeth_)

researcher(Fred_)
in-group(Fred_, TQY_)
in-project(Fred_, Elypse_)
hacker(Fred_)
works-with(Fred_,{Len_})

researcher(George_)
in-group(George_, TQY_)
in-project(George_, BCD_)
hacker(George_)
works-with(George_,{Bob_, Icabod_})

researcher(Harold_)
in-group(Harold_, TQY_)
in-project(Harold_, FUBAR_)
smoker(Harold_)
hacker(Harold_)

researcher(Icabod_)
in-group(Icabod_, TQY_)
heads-group(Icabod_, TQY_)
in-project(Icabod_, BCD_)
works-with(Icabod_,{Bob_, George_})

secretary(Jim_)
in-group(Jim_, TQY_)
works-with(Jim_,{Icabod_, Karen_, Marg_})

secretary(Karen_)
in-group(Karen_, TQY_)
works-with(Karen_,{Icabod_, Jim_, Marg_})

researcher(Len_)
in-group(Len_, TQY_)
in-project(Len_, Elypse_)
heads-project(Len_, Elypse_)
smoker(Len_)
works-with(Len_,{Fred_})

manager(Marg_)
in-group(Marg_, TQY_)
works-with(Marg_,{Icabod_, Karen_, Jim_})

researcher(Nancy_)
in-group(Nancy_, TQY_)
in-project(Nancy_, Groupware_)
heads-project(Nancy_, Groupware_)

researcher(Oley_)
in-group(Oley_, TQY_)
in-project(Oley_, Hardware_)
heads-project(Oley_, Hardware_)
smoker(Oley_)
hacker(Oley_)

// project descriptions
large-project(Elypse_)
large-project(Groupware_)
large-project(Hardware_)

// room sizes
small-room (C5413)
small-room (C5414)
small-room (C5415)
small-room (C5416)
large-room (C5417)
large-room (C5419)
large-room (C5420)
medium-room (C5421)
medium-room (C5422)
medium-room (C5423)

// room proximity
close(C5413, {C5414,C5415})
close(C5414, C5415)
close(C5414, C5416)
close(C5415, C5416)
close(C5415, C5417)
close(C5416, C5417)
close(C5417, C5419)
close(C5419, C5420)
close(C5419, C5421)
close(C5419, C5422)
close(C5420, C5421)
close(C5421, C5420)
close(C5421, C5422)
close(C5421, C5423)
