package cpsc433;
import java.util.Random;
import java.io.*;
public class ProblemGenerator {

	private int numEmployees;
	private int numRooms;	
	
	private int maxClose;
	private int maxWorksWith;
	private int numProjects;
	private int numGroups;
	
	private Random EmployeeGenerator = new Random();
	private Random RoomGenerator = new Random();
	private Random GenericRandom = new Random();
	private String [] roles = { "hacker", "secretary", "manager", "researcher" , "smoker"};
	private String [] roomType = { "small-room" , "medium-room", "large-room" };
	
	private String [] employeeNames;
	private String [] roomsNames;
	private String [] groupsNames;
	private String [] projectsNames;
	private String [] majorRoles = { "none" , "heads-group" , "heads-project"};
	
	public ProblemGenerator(int numEmps, int numRooms, int numGroups, int numProjects, 
							int maxClose, int workswith)
	{
		this.numEmployees = numEmps;
		this.numRooms = numRooms;
		this.employeeNames = new String[numEmps];
		this.roomsNames = new String[numRooms];
		this.numProjects = numProjects;
		this.numGroups = numGroups;
		this.groupsNames = new String[numGroups];
		this.projectsNames = new String[numProjects];
		this.maxClose = maxClose;
		this.maxWorksWith = workswith;
	}
	public void GenerateProblem() throws IOException
	{
		int index;

		FileWriter fstream = new FileWriter("out.txt");
		BufferedWriter out = new BufferedWriter(fstream);
		
		// generate group names
		for(index = 0; index < numGroups; index++)
		{
			String group = generateRandomString(GenericRandom);
			groupsNames[index] = group;
		}
		
		// generate project names
		for(index = 0; index < numProjects; index++)
		{
			String project = generateRandomString(GenericRandom);
			projectsNames[index] = project;
		}
		
		// create employees and assign roles, groups and projects
		out.write("// Employees and roles");
		out.newLine();
		out.newLine();
		createEmployees(out);		
		
		// create worksWith relation
		out.write("// Works with relations");
		out.newLine();
		out.newLine();
		worksWithRelation(out);
		
		// create the rooms
		out.write("// Rooms");
		out.newLine();
		out.newLine();
		createRooms(out);
		
		// create closeness relations for rooms
		closenessRelation(out);
		
		// create some large projects
		out.write("// Large Projects");
		out.newLine();
		out.newLine();
		createLargeProjects(out);


		out.close();
	}

	private String generateRandomString(Random random)
	{
		char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < 5; i++) {
		    char c = chars[random.nextInt(chars.length)];
		    sb.append(c);
		}
		return sb.toString();
	}
	private void worksWithRelation(BufferedWriter out) throws IOException {
		// TODO Auto-generated method stub
		for(int i = 0; i < numEmployees; i++)
		{	
			
			int worksWith = GenericRandom.nextInt(maxWorksWith);
			
			for(int j = 0 ; j < worksWith ; j++)
			{
				int worksWithEmployee = EmployeeGenerator.nextInt(numEmployees);				
				if(i != worksWithEmployee)
				{
					String predicate = "works-with(" + employeeNames[i] + "," + employeeNames[worksWithEmployee] + ")";
					out.write(predicate);
					out.newLine();		
				}
			}
		}
	}
	private void createEmployees(BufferedWriter out) throws IOException
	{
		for(int index = 0; index < numEmployees; index++)
		{
			String employee = generateRandomString(EmployeeGenerator); //Integer.toString(EmployeeGenerator.nextInt());
			String predicate;
			employeeNames[index] = employee;
			int numRoles = GenericRandom.nextInt(2)  + 1 ;	// max of 2 roles per employee, minimum 1
			for(int i = 0; i < numRoles ; i++)
			{
				int indexRole = GenericRandom.nextInt(roles.length);
				predicate = roles[indexRole] + "(" + employee + ")";
				out.write(predicate);
				out.newLine();
			}
			// give a group and a project to that employee 
			// make employee a GH or PH or none
			int otherRole =  GenericRandom.nextInt(majorRoles.length);
			if(numGroups > 0)
			{
				int groupIndex = GenericRandom.nextInt(numGroups);
				String groupName = groupsNames[groupIndex];
				predicate = "group(" + employee + ", " + groupName + ")";
				out.write(predicate);
				out.newLine();
				if (otherRole == 1)
				{
					assignImportantRole(majorRoles[otherRole], employee, groupName, out);
				}
			}
			
			if(numProjects > 0)
			{
				int projectIndex = GenericRandom.nextInt(numProjects);
				String projectName = projectsNames[projectIndex];
				predicate = "project(" + employee + ", " + projectName + ")";
				out.write(predicate);
				out.newLine();
				if (otherRole == 2)
				{
					assignImportantRole(majorRoles[otherRole], employee, projectName, out);
				}
			}						
		}		
	}
	
	private void createLargeProjects(BufferedWriter out) throws IOException
	{
		int numLargeProjects = GenericRandom.nextInt(numProjects);
		for (int i = 0; i < numLargeProjects; i++)
		{
			int projectIndex = GenericRandom.nextInt(numProjects);
			String predicate = "large-project(" + projectsNames[projectIndex] + ")";
			out.write(predicate);
			out.newLine();
		}
	}
	
	private void assignImportantRole(String role, String employee, String groupOrProject,BufferedWriter out) throws IOException
	{
		String predicate = role + "(" + employee + " , " + groupOrProject +" )"; 
		out.write(predicate);
		out.newLine();
	}
	private void createRooms(BufferedWriter out) throws IOException
	{
		for(int index = 0; index < numRooms; index++)
		{			
			String roomName = generateRandomString(RoomGenerator);
			roomsNames[index] = roomName;
			int indexRoomSize = GenericRandom.nextInt(roomType.length);
			String predicate = roomType[indexRoomSize] + "(" + roomName + ")";
			out.write(predicate);
			out.newLine();
		}		
	}
	private void closenessRelation(BufferedWriter out) throws IOException
	{
		 for (int index = 0 ; index < numRooms ; index++)
		 {
			 String roomName = roomsNames[index];
			 int numClose = GenericRandom.nextInt(maxClose);
			 
			 for(int i = 0; i < numClose; i++)
			 {
				 int roomCloseIndex = GenericRandom.nextInt(numRooms);
				 if(index != roomCloseIndex){
					 String predicate = "close(" + roomName + "," + roomsNames[roomCloseIndex] + ")";
					 out.write(predicate);
					 out.newLine();
				 }
			 }
		 }
	}
}
