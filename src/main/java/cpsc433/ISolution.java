package cpsc433;

public interface ISolution {

	String getName();

	boolean isSolved();

	int getGoodness();

	boolean isComplete();

}
