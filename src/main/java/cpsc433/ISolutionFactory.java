package cpsc433;

public interface ISolutionFactory {

	ISolution fromFile(String string);

}
