package cpsc433;

public interface IEnvironment {
  ISolution getCurrentSolution();
  void setCurrentSolution(ISolution currentSolution);
  void a_search(String search, String control, Long maxTime);
  int fromFile(String fromFile);
  void assert_(String pred);
  boolean eval(String pred);
}
