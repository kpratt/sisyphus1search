package cpsc433.solution

object EnvData {
  def apply(): Env = Env(Map(), Map(), Map())
  def quote(str: String) = "\"" + str + "\""
}

case class Env(employees: Map[String, Employee], rooms: Map[String, Room], projects: Map[String, Project]) {
  
  lazy val groups: Map[String, Group] = {
    val groups = employees.
      map(_._2.group).
      foldRight(List[String]())((a,b) => if(!b.contains(a)) a::b else b ).
      foldRight(Map[String, Group]()) { 
        (s:String, m:Map[String,Group]) => 
          m.+(  (s, Group(s, "", List[String]()))  ) 
      }
    
    employees.values.foldRight(groups) {
      (e, gs) =>
        val update = gs(e.group).addMember(e.name)
        gs.+(e.group -> update)
    }
  } 
  
  lazy val desks: Int = rooms.values.map(_.spareRoom).foldRight(0)(_+_)
  lazy val (roomHogs, sharerers) = employees.values.partition(_.getsThierOwnRoom)
  lazy val spareDesks = desks - ((roomHogs.size * 2) + sharerers.size)
  
  lazy val secrataries = employees.values.filter(_.isSecretary)
  lazy val groupHeads = employees.values.filter(_.isGroupHead)
  lazy val projectHeads = employees.values.filter(_.isProjectHead)
  lazy val managers = employees.values.filter(_.isManager)
  lazy val fullRooms = rooms.values.filter(_.spareRoom == 0)
  
  def findOrCreateEmployee(name: String) = employees.get(name).getOrElse(Employee.create(name))
  def put(e: Employee) = Env(employees + (e.name -> e), rooms, projects)
  def transformE(name: String, f: Employee => Employee) = put(f(findOrCreateEmployee(name)))
  def testE(name: String, f: (Employee) => Boolean): Boolean = {
    employees.get(name).map(f(_)).getOrElse(false)
  }

  def findOrCreateRoom(name: String) = rooms.get(name).getOrElse(Room(name, Small, (None, None), List()))
  def put(r: Room) = Env(employees, rooms + (r.name -> r), projects)
  def transformR(name: String, f: Room => Room) = put(f(findOrCreateRoom(name)))
  def testR(name: String, f: (Room) => Boolean): Boolean = {
    rooms.get(name).map(f(_)).getOrElse(false)
  }

  def findOrCreateProject(name: String) = projects.get(name).getOrElse(Project(name, Small))
  def put(p: Project) = Env(employees, rooms, projects + (p.name -> p))
  def transformP(name: String, f: Project => Project) = put(f(findOrCreateProject(name)))
  def testP(name: String, f: (Project) => Boolean): Boolean = {
    projects.get(name).map(f(_)).getOrElse(false)
  }
  
  lazy val isSolvable: Boolean = {
    val managerCount = employees.values.filter(x => x.isManager || x.isProjectHead || x.isGroupHead).size
    val employeeCount = employees.values.size - managerCount
    (rooms.size - managerCount) * 2 >= employeeCount
  }  

  override def equals(other: Any) = {
    if(other == null) false else {
      val that = other.asInstanceOf[Env]
      if(that == null) false else {
        employees.equals(that.employees) &&
        rooms.equals(that.rooms) &&
        projects.equals(that.projects)
      }  
    }
  }
  
  override def toString: String = {
    val buf = new StringBuilder

    employees.values.map(_.toString).addString(buf)
    rooms.values.map(_.toString).addString(buf)
    projects.values.map(_.toString).addString(buf)

    buf.toString()
  }
  
}

case class Group(name: String, head: String, members: List[String]){
  def setHead(hName: String): Group = Group(name, hName, members)
  def addMember(mName: String): Group = Group(name, head, mName :: members)
}

abstract class Size
case object Large extends Size {
  override def toString() = "large"
}
case object Medium extends Size{
  override def toString() = "medium"
}
case object Small extends Size{
  override def toString() = "small"
}

case class Project(
  val name: String,
  val size: Size) {
  def setSize(s: Size) = Project(name, s)
  
  override def toString() = {
    val buff = new StringBuilder()
    buff.append("project("+EnvData.quote(name)+")").append("\n")
    if(size.eq(Large))buff.append("large-project("+EnvData.quote(name)+")").append("\n")
    buff.append("\n").toString()
  }
}

case class Room(
  val name: String,
  val size: Size,
  val occupants: (Option[String], Option[String]),
  val closeTo: List[String]) {

  lazy val spareRoom = (2 - (occupants._1.toList ++ occupants._2.toList).size)
  
  def setSize(s: Size) = Room(name, s, occupants, closeTo)
  def addCloseTo(other: String) = Room(name, size, occupants, other :: closeTo)
  def addOccupant(emp: String): Either[String, Room] = {
    if(occupantList.contains(emp)){
      Right(this)
    } else if(occupants._1.isEmpty) {
      Right(Room(name, size, (Some(emp), occupants._2), closeTo))
    } else if(occupants._2.isEmpty) {
      Right(Room(name, size, (occupants._1, Some(emp)), closeTo))
    } else {
      Left("error: room(" + name + ") is already full.")
    }
  }
  
  def occupantList = occupants._1.toList ++ occupants._2.toList
  
  override def toString() = {
    val buff = new StringBuilder()
    buff.append("room("+EnvData.quote(name)+")").append("\n")
    buff.append(size.toString()+"-room("+EnvData.quote(name)+")").append("\n")
    if(!closeTo.isEmpty)buff.append("close("+EnvData.quote(name)+", "+closeTo.map(EnvData.quote(_)).mkString("{", ",", "}")+")").append("\n")
    occupantList.map("assigned-to("+EnvData.quote(_)+", "+EnvData.quote(name)+")\n").addString(buff)
    buff.append("\n").toString()
  }
}

object Employee {
  def create(name: String) =
    Employee(name, "", "", List[String](), Option.empty[String], false, false, false, false, false, false, false)
}

case class Employee(
  val name: String,
  project: String,
  group: String,
  worksWith: List[String],
  assignedTo: Option[String],
  isProjectHead: Boolean,
  isGroupHead: Boolean,
  isHacker: Boolean,
  isResearcher: Boolean,
  isSecretary: Boolean,
  isManager: Boolean,
  isSmoker: Boolean) {
  
  lazy val getsThierOwnRoom: Boolean = isProjectHead || isGroupHead || isManager
  
  def setProject(p: String) = Employee(
    name, p, group, worksWith, assignedTo,
    isProjectHead, isGroupHead, isHacker, isResearcher, isSecretary, isManager, isSmoker)

  def setGroup(g: String) = Employee(
    name, project, g, worksWith, assignedTo,
    isProjectHead, isGroupHead, isHacker, isResearcher, isSecretary, isManager, isSmoker)

  def addWorksWith(ww: String) = Employee(
    name, project, group, ww :: worksWith, assignedTo,
    isProjectHead, isGroupHead, isHacker, isResearcher, isSecretary, isManager, isSmoker)

  def setRoom(r: String) = Employee(
    name, project, group, worksWith, Some(r),
    isProjectHead, isGroupHead, isHacker, isResearcher, isSecretary, isManager, isSmoker)

  def setProjectHead(tru: Boolean) = Employee(
    name, project, group, worksWith, assignedTo,
    tru, isGroupHead, isHacker, isResearcher, isSecretary, isManager, isSmoker)

  def setGroupHead(tru: Boolean) = Employee(
    name, project, group, worksWith, assignedTo,
    isProjectHead, tru, isHacker, isResearcher, isSecretary, isManager, isSmoker)

  def setHacker(tru: Boolean) = Employee(
    name, project, group, worksWith, assignedTo,
    isProjectHead, isGroupHead, tru, isResearcher, isSecretary, isManager, isSmoker)

  def setResearcher(tru: Boolean) = Employee(
    name, project, group, worksWith, assignedTo,
    isProjectHead, isGroupHead, isHacker, tru, isSecretary, isManager, isSmoker)

  def setSecretary(tru: Boolean) = Employee(
    name, project, group, worksWith, assignedTo,
    isProjectHead, isGroupHead, isHacker, isResearcher, tru, isManager, isSmoker)

  def setManager(tru: Boolean) = Employee(
    name, project, group, worksWith, assignedTo,
    isProjectHead, isGroupHead, isHacker, isResearcher, isSecretary, tru, isSmoker)

  def setSmoker(tru: Boolean) = Employee(
    name, project, group, worksWith, assignedTo,
    isProjectHead, isGroupHead, isHacker, isResearcher, isSecretary, isManager, tru)

  override def toString() = {
    val buff = new StringBuilder()
    buff.append("person("+EnvData.quote(name)+")").append("\n")
    if(isHacker)buff.append("hacker("+EnvData.quote(name)+")").append("\n")
    if(isSecretary)buff.append("secretary("+EnvData.quote(name)+")").append("\n")
    if(isSmoker)buff.append("smoker("+EnvData.quote(name)+")").append("\n")
    if(isManager)buff.append("manager("+EnvData.quote(name)+")").append("\n")
    if(isResearcher)buff.append("researcher("+EnvData.quote(name)+")").append("\n")
    if(isProjectHead)buff.append("heads-project("+EnvData.quote(name)+", "+EnvData.quote(project)+")").append("\n")
    if(isGroupHead)buff.append("heads-group("+EnvData.quote(name)+", "+EnvData.quote(group)+")").append("\n")
    buff.append("in-group("+EnvData.quote(name)+", "+EnvData.quote(group)+")").append("\n")
    buff.append("in-project("+EnvData.quote(name)+", "+EnvData.quote(project)+")").append("\n")
    
    if(!worksWith.isEmpty)buff.append("works-with("+EnvData.quote(name)+", "+worksWith.map(EnvData.quote(_)).mkString("{", ",", "}")+")").append("\n")
    assignedTo map(room => "assigned-to("+EnvData.quote(room)+")") foreach { str => buff.append(str).append("\n") }
    
    buff.append("\n").toString()
  }
}


