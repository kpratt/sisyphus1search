package cpsc433.solution

import cpsc433.ISolution
import scala.collection.mutable.ArrayBuffer
import scala.util.Random
import cpsc433.benchmark.BenchmarkService
import scala.collection.mutable.PriorityQueue
import scala.collection.immutable.HashSet

object Genetics {

  def makeIntoSolution(ppl: Seq[Option[Employee]], rooms: Seq[Room]): (Env) => Env = {
    def recurse(ppl: Seq[Option[Employee]], rooms: Seq[Room]): List[(Room, Option[Employee], Option[Employee])] =
      rooms match {
        case rm :: rms =>
          val roomInTheRoom = rm.spareRoom
          //TODO Test potential edge cases
          ppl match {
            case p :: q :: rest if (roomInTheRoom == 2 && !p.map(_.getsThierOwnRoom).getOrElse(false) && !q.map(_.getsThierOwnRoom).getOrElse(false)) =>
              (rm, p, q) :: recurse(rest, rms)
            case p :: rest if (roomInTheRoom == 2 && p.map(_.getsThierOwnRoom).getOrElse(false)) =>
              (rm, p, None) :: recurse(rest, rms)
            case p :: q :: rest if (roomInTheRoom == 2 && q.map(_.getsThierOwnRoom).getOrElse(false)) =>
              (rm, q, None) :: recurse(p :: rest, rms)
            case p :: rest if (roomInTheRoom == 1 && !p.map(_.getsThierOwnRoom).getOrElse(false)) =>
              (rm, p, None) :: recurse(rest, rms)
            case p :: rest if (roomInTheRoom == 1 && p.map(_.getsThierOwnRoom).getOrElse(false)) =>
              recurse(rest ++ List(p), rm :: rms)
            case Nil => Nil
            case rest if (roomInTheRoom == 0) => recurse(rest, rms)
            case p :: Nil if roomInTheRoom >= 1 => (rm, p, None) :: recurse(Nil, rms)
            case _ => throw new RuntimeException("Fuuuuccckkk! " + ppl.toString + "\n" + rm.toString())
          }
        case Nil => Nil
      }
    //BenchmarkService.start("Make Into Solution")
    val ret = recurse(ppl, rooms).foldRight[(Env) => Env](identity) {
      (a, b) =>
        def f(env: Env) = env.transformR(a._1.name, r => {
          (a._2.toList ++ a._3.toList).foldRight(r) {
            (a, b) =>
              b.addOccupant(a.name).right.get
          }
        })
        def g(env: Env) = a._2.map(emp => env.transformE(emp.name, e => e.setRoom(a._1.name))).getOrElse(env)
        def h(env: Env) = a._3.map(emp => env.transformE(emp.name, e => e.setRoom(a._1.name))).getOrElse(env)
        b.compose(f).compose(g).compose(h)
    }
    //BenchmarkService.end("Make Into Solution")
    ret
  }

  def genomeSize(env: Env): Int = {
    val (bosses, subs) = env.employees.values.partition(_.getsThierOwnRoom)
    bosses.size + ((env.rooms.size - bosses.size) * 2)
  }

  val random = new Random()

  def generateN(count: Int, size: Int) = (0 until count).map(x => generate(size))

  def generate(size: Int): Genome =
    Genome((1 to size).map(random.nextInt(_)).reverse.toList)

  def sequenceN[A](f: A => A)(n: Int): A => A = n match {
    case 0 => identity
    case 1 => f
    case i => f compose sequenceN(f)(n - 1)
  }

  def mutate(g: Genome): Genome = {
    //BenchmarkService.start("Mutation")
    val i = random.nextInt(g.selectors.size)
    val r = random.nextInt(g.selectors.size - i)
    def replaceAt(s: List[Int], n: Int, rplmt: Int): List[Int] = s match {
      case h :: t if (n == 0) => rplmt :: t
      case h :: t => h :: replaceAt(t, n - 1, rplmt)
      case Nil => Nil
    }
    val ret = Genome(replaceAt(g.selectors, i, r))
    //BenchmarkService.end("Mutation")
    ret
  }

  def crossover(splitPoint: Int)(mom: Genome, dad: Genome): Genome = {
    //BenchmarkService.start("Crossover")
    val (ml, mr) = mom.selectors.splitAt(splitPoint)
    val (dl, dr) = dad.selectors.splitAt(splitPoint)
    val ret = Genome(ml ++ dr)
    //BenchmarkService.end("Crossover")
    ret
  }

  def diversity(pop: List[Genome]): Seq[Int] = {
    val genomeSize = pop.headOption.map(_.selectors.size) getOrElse (0)
    val spaces = (0 until genomeSize).toList
    val seed: List[List[Int]] = spaces.map(_ => List())
    val matrix = pop.map(_.selectors).foldRight(seed)((a, b) => a.zip(b).map(x => x._1 :: x._2))
    matrix.map(_.foldRight(HashSet[Int]())((a, b) => b + a)).map(_.size)
  }
  
  val PERCENT_TIME_WITH_BIASED_GENOCIDE = 0.8
  val PERCENT_TIME_IMPROVING_ON_THE_BEST = 0.6
  val MUTATION_LIMIT_PER_CYCLE = 0.2
}

object Orderings {
  implicit def solutionOrdering: Ordering[(Genome, ISolution)] = Ordering.fromLessThan(_._2.getGoodness() < _._2.getGoodness())
}

class GenePool(var pop: Seq[Genome], val popLimit: Int, val env: Env) {
  import Orderings.solutionOrdering

  val genomeSize = pop.head.selectors.size
  val staticRooms = env.rooms.values.toList
  val staticPeople = env.employees.values.map(Some(_)).toList ++ (for (i <- 0 to env.spareDesks) yield None)
  val random = new Random()

  var best: Option[(Genome, ISolution)] = None

  def simpleSearch(timeout: Long) = {
    var crossover = Genetics.crossover(genomeSize / 2)(_, _)
    var mutate: Genome => Genome = Genetics.sequenceN(Genetics.mutate)(1)
    var iters = pop.size
    var queue = pop.foldRight(PriorityQueue[(Genome, ISolution)]()) {
      (a, b) => b.+=((a, develop(a)))
    }

    val runtime = timeout - System.currentTimeMillis()
    var timeLeft = runtime
    var timeThreshold = runtime * 0.6 // arbitrary  
    while (timeLeft > 0) {
      crossover = Genetics.crossover((genomeSize * (timeLeft / runtime) * 0.5).toInt)(_, _)
      mutate = Genetics.sequenceN(Genetics.mutate)(((((runtime - timeLeft) / runtime) * genomeSize * Genetics.MUTATION_LIMIT_PER_CYCLE) + 1).toInt)
      //BenchmarkService.start("Outer Loop")
      val array: Array[(Genome, ISolution)] = queue.toArray
      // develop the population
      while (queue.size < popLimit) {
        //BenchmarkService.start("Inner Loop")
        val i = random.nextInt(array.size)
        val mommy = array(i)._1
        val daddy = selectRandomOrBest(array, timeLeft, runtime)
        val child = crossover(mommy, daddy)
        queue += ((child, develop(child)))

        val k = random.nextInt(array.size)
        val mutant = mutate(selectRandomOrBest(array, timeLeft, runtime))
        queue += ((mutant, develop(mutant)))
/*
        val randomIndividual: Genome = Genetics.generate(genomeSize)
        queue += ((randomIndividual, develop(randomIndividual)))
*/
        iters += 3
        //BenchmarkService.end("Inner Loop")
      }

      val bestAlive = queue.headOption
      val baGood = bestAlive map (_._2.getGoodness()) getOrElse (Int.MinValue)
      val bestGood = best map (_._2.getGoodness()) getOrElse (Int.MinValue)
      if (baGood > bestGood) {
//        println("++++")
        best = bestAlive
        println("Time: " + (runtime - timeLeft) + ", Best utility: " + best.map(_._2.getGoodness()).getOrElse(1))
      }
      val list: List[Genome] = queue.toList.map(_._1)
      val divers = Genetics.diversity(list)
      //println("time left ["+timeLeft+"] genetic diversity breakdown: " + divers.map(_.toString()).toString)
      queue = genocide(queue, timeLeft, runtime)
      //recalculate time left
      timeLeft = timeout - System.currentTimeMillis()
      //BenchmarkService.end("Outer Loop")
    }
    println("Number of successful explorations: " + iters.toString() + ", Best utility: " + best.map(_._2.getGoodness()).getOrElse(1))
  }

  def selectRandomOrBest(array: Array[(Genome, ISolution)], timeLeft: Long, runtime: Long): Genome = {
    val i = random.nextInt(array.size)
    lazy val randomSelect = array(i)
    lazy val bestShot = ((runtime - timeLeft) / runtime) * 80
    if (timeLeft < runtime * Genetics.PERCENT_TIME_IMPROVING_ON_THE_BEST && random.nextInt(2) < 1) { //}){
      best.getOrElse(randomSelect)._1
    } else {
      randomSelect._1
    }
  }

  def genocide(q: PriorityQueue[(Genome, ISolution)], timeLeft: Long, runtime: Long): PriorityQueue[(Genome, ISolution)] = {
    var ret: PriorityQueue[(Genome, ISolution)] = null
    if (timeLeft <= runtime * Genetics.PERCENT_TIME_WITH_BIASED_GENOCIDE) {
      //BenchmarkService.start("Shuffle")
      ret = q.dropRight(popLimit - 50)
      //BenchmarkService.end("Shuffle")
    } else {
      //BenchmarkService.start("Drop")
      ret = q.foldRight(PriorityQueue())((pair, q) => if (random.nextInt(2) == 1) q += pair else q)
      //BenchmarkService.end("Drop")
    }
    ret
  }

  def develop(genome: Genome): ISolution =
    new Solution(
      Genetics.makeIntoSolution(
        genome.produceOrdering(staticPeople.map(_.map(_.name).getOrElse(""))).
          map(env.employees.get(_)), staticRooms)(env))
}

case class Genome(selectors: List[Int]) {

  def produceOrdering(staticPeople: Seq[String]): List[String] = {
    def recurs(sel: List[Int], peeps: Seq[String]): List[String] = {
      if (peeps.size > 0) {
        sel match {
          case head :: tail => {
            val chosen = peeps.apply(head % peeps.size)
            chosen :: recurs(tail, peeps.filter(_ != chosen))
          }
          case Nil => Nil
        }
      } else {
        Nil
      }
    }
    //BenchmarkService.start("Produce Ordering")
    val ret = recurs(selectors, staticPeople)
    //BenchmarkService.end("Produce Ordering")
    ret
  }

}