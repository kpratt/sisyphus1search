package cpsc433.solution
import cpsc433.ISolution
import cpsc433.benchmark.BenchmarkService

object Solution {
  def apply() = new Solution(new Env(Map(), Map(), Map()))
  def apply(env: Env) = new Solution(env)
}
class Solution(env: Env) extends ISolution {

  override def toString: String = {
    val buf = new StringBuilder

    if (!env.isSolvable) buf.append("//This problem could not be solved\n\n")
    buf.append(env.toString)

    buf.toString()
  }

  def getName() = "Planet Bob"

  def isSolved() = true

  lazy val colseTos: Map[Employee, List[Employee]] = env.employees.values.filter(_.getsThierOwnRoom).foldRight(Map[Employee, List[Employee]]()){
    (a: Employee, b: Map[Employee, List[Employee]]) => b.+((a, peopleCloseTo(a)))
  }
  
  //def getGoodness() = Int.MinValue
  def getGoodness(): Int = goodness
  lazy val goodness: Int = {
    //BenchmarkService.start("Goodness")
    val ret = List[() => (Int, Int)](
      penalizeGroupHeadsFarFromLargeProjectHeads,
      penalizeGroupMembersFarFromGroupHead,
      penalizeHackersSharingOfficesWithNonHackers,
      penalizeManagersFarFromGroupHead,
      penalizeManagersFarFromGroupMembers,
      penalizeMembersFarFromThierProjectHeads,
      penalizeMembersOfSameProjectSharingOffice,
      penalizePeopleSharingOfficeAndDontWorkTogether,
      penalizePeopleSharingOffices,
      penalizePeopleSharingSmallRoom,
      penalizeSecretaryFarGH,
      penalizeSecretaryFarM,
      penalizeSecretaryFarPH,
      penalizeSecretaryNonShare,
      penalizeSmallOfficeGH,
      penalizeSmokerWithNon).map(_()).map(x => x._1 * x._2).foldRight(0)(_ + _)
    //BenchmarkService.end("Goodness")
    ret
  }

  def isComplete() = false

  //12

  def penalizeMembersOfSameProjectSharingOffice(): (Int, Int) = {
    //BenchmarkService.start("SC12")
    val count =
      (for {
        room <- env.fullRooms
        employee_name1 <- room.occupants._1.toList
        employee_name2 <- room.occupants._2.toList
        emp1 <- env.employees.get(employee_name1)
        emp2 <- env.employees.get(employee_name2)
        if (emp1.project == emp2.project)
      } yield room.name).size
    //BenchmarkService.end("SC12")
    (count * 2, -7)
  }

  //13
  def penalizeHackersSharingOfficesWithNonHackers(): (Int, Int) = {
    //BenchmarkService.start("SC13")
    val count =
      (for {
        room <- env.fullRooms
        employee_name1 <- room.occupants._1.toList
        employee_name2 <- room.occupants._2.toList
        emp1 <- env.employees.get(employee_name1)
        emp2 <- env.employees.get(employee_name2)
        if (!emp1.isSecretary && !emp2.isSecretary &&
          ((emp1.isHacker && !emp2.isHacker) ||
            (!emp1.isHacker && emp2.isHacker)))
      } yield room.name).size
    //BenchmarkService.end("SC13")
    (count * 2, -2)
  }

  // 14
  def penalizePeopleSharingOffices(): (Int, Int) = {
    //BenchmarkService.start("SC14")
    val count =
      (for {
        room <- env.fullRooms
      } yield room.name).size
    //BenchmarkService.end("SC14")
    (count * 2, -4)
  }

  //15 
  def penalizePeopleSharingOfficeAndDontWorkTogether(): (Int, Int) = {
    //BenchmarkService.start("SC15")
    val count =
      (for {
        room <- env.fullRooms
        employee_name1 <- room.occupants._1.toList
        employee_name2 <- room.occupants._2.toList
        emp1 <- env.employees.get(employee_name1)
        emp2 <- env.employees.get(employee_name2)
        if (!emp1.worksWith.contains(emp2))
      } yield room.name).size
    //BenchmarkService.end("SC15")
    (count * 2, -3)

  }

  // 16
  def penalizePeopleSharingSmallRoom(): (Int, Int) = {
    //BenchmarkService.start("SC16")
    val count =
      (for {
        room <- env.fullRooms
        if (room.size == Small)
      } yield room.name).size
    //BenchmarkService.end("SC16")
    (count * 2, -25)
  }

  //// Soft constraints 2,6,7,8,10
  def peopleCloseTo(origin: String): List[Employee] =
    env.rooms.get(origin).map(peopleCloseTo(_)).getOrElse(List())

  def peopleCloseTo(person: Employee): List[Employee] =
    for {
      room <- person.assignedTo.flatMap(r => env.rooms.get(r)).toList
      neighbor <- peopleCloseTo(room)
    } yield neighbor

  lazy val bosses = env.employees.values.filter(_.getsThierOwnRoom)
  lazy val closeCache: Map[Employee, List[Employee]] = bosses.map(x => (x -> peopleCloseTo(x))).toMap 
  
  def peopleCloseTo(origin: Room): List[Employee] = {
    //BenchmarkService.start("CloseTo")
    val ret = for {
      rm <- (origin.name :: origin.closeTo).toList
      room <- env.rooms.get(rm).toList
      occupant <- room.occupantList
      emp <- env.employees.get(occupant).toList
    } yield emp
    //BenchmarkService.end("CloseTo")
    ret
  }

  //2
  def penalizeGroupMembersFarFromGroupHead(): (Int, Int) = {
    //BenchmarkService.start("SC02")
    val count = env.employees.size -
      (for {
        emp <- env.groupHeads
        groupMember <- closeCache.get(emp).getOrElse(List())
        if (groupMember.group == emp.group)
      } yield groupMember).size
    //BenchmarkService.end("SC02")
    (count, -2)
  }
  //6
  def penalizeManagersFarFromGroupHead(): (Int, Int) = {
    //BenchmarkService.start("SC06")
    val managers = env.employees.values.filter(_.isManager)
    val count = managers.size - (for {
      emp <- env.groupHeads
      groupMember <- closeCache.get(emp).getOrElse(List())
      if(groupMember.isManager)
      if(groupMember.group == emp.group)
    } yield groupMember).size
    //BenchmarkService.end("SC06")
    (count, -20)
  }

  //7
  def penalizeManagersFarFromGroupMembers(): (Int, Int) = {
    //BenchmarkService.start("SC07")
    val possible = env.groups.values.foldRight(0) {
      (group, n) =>
        val groupM = (for {
          member <- group.members
          emp <- env.employees.get(member)
          if (emp.isManager)
        } yield emp)
        n + ((group.members.size - 1) * groupM.size)
    }

    val non_violations = (for {
      emp <- env.managers
      groupMember <- closeCache.get(emp).getOrElse(List())
      if (groupMember.group == emp.group && emp != groupMember)
    } yield groupMember).size
    //BenchmarkService.end("SC07")
    (possible - non_violations, -2)
  }

  //8
  def penalizeMembersFarFromThierProjectHeads(): (Int, Int) = {
    //BenchmarkService.start("SC08")
    val count = env.employees.size - (for {
      emp <- env.projectHeads
      groupMember <- closeCache.get(emp).getOrElse(List())
      if (groupMember.project == emp.project)
    } yield groupMember).size
    //BenchmarkService.end("SC08")
    (count, -5)
  }

  //10
  def penalizeSmokerWithNon(): (Int, Int) = {
    //BenchmarkService.start("SC10")
    val count =
      (for {
        room <- env.fullRooms
        employee_name1 <- room.occupants._1.toList
        employee_name2 <- room.occupants._2.toList
        emp1 <- env.employees.get(employee_name1)
        emp2 <- env.employees.get(employee_name2)
        if (emp1.isSmoker ^ emp2.isSmoker) // XOR == ^ 
      } yield room.name).size
    //BenchmarkService.end("SC10")
    (count, -50)
  }

  //1
  def penalizeSecretaryNonShare(): (Int, Int) = {
    //BenchmarkService.start("SC01")
    val count =
      (for {
        room <- env.fullRooms
        employee_name1 <- room.occupants._1.toList
        employee_name2 <- room.occupants._2.toList
        emp1 <- env.employees.get(employee_name1)
        emp2 <- env.employees.get(employee_name2)
        if (emp1.isSecretary ^ emp2.isSecretary) // XOR == ^ 
      } yield room.name).size
    //BenchmarkService.end("SC01")
    (count, -5)
  }

  //3
  def penalizeSmallOfficeGH(): (Int, Int) = {
    //BenchmarkService.start("SC03")
    val count =
      (for {
        grouphead <- env.groupHeads
        roomName <- grouphead.assignedTo.toList
        room <- env.rooms.get(roomName)
        if (room != null && !(room.size == Large))
      } yield grouphead.name).size
    //BenchmarkService.end("SC03")
    (count, -40)
    //(1,-40)
  }

  //4
  def penalizeSecretaryFarGH(): (Int, Int) = { // secrataries far form group head or group heads far from A secratary?
    //BenchmarkService.start("SC04")
    val groupHeads = env.groupHeads
    val count = groupHeads.size - (for {
      groupHead <- env.groupHeads
      groupMember <- closeCache.get(groupHead).getOrElse(List())
      if (groupMember.isSecretary && groupMember.group == groupHead.group)
    } yield groupMember).size
    //BenchmarkService.end("SC04")
    (count, -30)
  }

  // 9 -- only applies for heads of large projects
  def penalizeSecretaryFarPH(): (Int, Int) = { // secrataries far form project head or project heads far from A secratary?
    //BenchmarkService.start("SC09")
    val projectHeadsOfLP = (for {
      emp <- env.projectHeads
      project <- env.projects.get(emp.project)
      if (project.size == Large)
    } yield emp)

    val count = projectHeadsOfLP.size - (for {
      emp <- projectHeadsOfLP
      project <- env.projects.get(emp.project).toList
      groupMember <- closeCache.get(emp).getOrElse(List())
      if (groupMember.isSecretary)
      if (groupMember.group == emp.group)
    } yield groupMember).size
    //BenchmarkService.end("SC09")
    (count, -10)
  }

  //5
  def penalizeSecretaryFarM(): (Int, Int) = {
    //BenchmarkService.start("SC05")
    val managers = env.managers
    val count = managers.size - (for {
      manager <- env.managers
      closeEmp <- closeCache.get(manager).getOrElse(List())
      if (closeEmp.isSecretary &&
        closeEmp.group == manager.group)
    } yield manager.name).size
    //BenchmarkService.end("SC05")
    (count, -20)
  }

  ///11
  def penalizeGroupHeadsFarFromLargeProjectHeads(): (Int, Int) = {
//BenchmarkService.start("SC11")
    var x = 0
	var y = 0
    
    for {
      emp <- env.projectHeads
      project <- env.projects.get(emp.project).toList
      if(project.size == Large)
      
    } {
	
	  x+=1
	  for{
        projectMember <- closeCache.get(emp).getOrElse(List())
        if(projectMember.isGroupHead)
        if(projectMember.project == emp.project)
	  } {
	    y+=1
	  }
	}
	val count = x - y
//BenchmarkService.end("SC11")
    (count, -10)
  }

}