package cpsc433.solution

import cpsc433.SisyphusI

object Main {
  def main(args: Array[String]) =
    SisyphusI.search(args, new Environment(), SolutionFactory)  

}