package cpsc433.solution

import cpsc433.Predicate.ParamType
import cpsc433.Predicate
import cpsc433.PredicateReader
import cpsc433.SisyphusPredicates
import cpsc433.IEnvironment
import cpsc433.ISolution
import cpsc433.Pair
import java.util.Set
import scala.collection.JavaConverters._
import java.io.InputStream

object Environment {
  //def get() = new Environment();
}

class Environment extends PredicateReader("Environment") with SisyphusPredicates with IEnvironment {

  var currentSolution: Option[ISolution] = None 
  
  def getCurrentSolution() = currentSolution.getOrElse(null)
  
  def setCurrentSolution(currentSolution: ISolution) {
    this.currentSolution = Some(currentSolution)
    
  }

  def a_search(search: String, control: String, maxTime: java.lang.Long) {
    val env = this.env;
	if(env.isSolvable){
    val size = Genetics.genomeSize(env)
      val pool = new GenePool(Genetics.generateN(50, size), 400, env)
      pool.simpleSearch(System.currentTimeMillis() + maxTime - 1000)
	  setCurrentSolution(pool.best.map(_._2).get)
	  } else {
	  println("Unsolvable")
	  }
  }
  
  
  var env = Env(Map(), Map(), Map())

  //predicate responses

  def a_person(name: String) { env = env.transformE(name, identity) }
  def a_room(name: String) { env = env.transformR(name, identity) }
  def a_project(name: String) { env = env.transformP(name, identity) }
  def a_group(name: String) {}

  def a_secretary(name: String) { env = env.transformE(name, _.setSecretary(true)) }
  def a_manager(name: String) { env = env.transformE(name, _.setManager(true)) }
  def a_researcher(name: String) { env = env.transformE(name, _.setResearcher(true)) }

  def a_smoker(name: String) { env = env.transformE(name, _.setSmoker(true)) }
  def a_hacker(name: String) { env = env.transformE(name, _.setHacker(true)) }

  def a_group(name: String, group: String) { env = env.transformE(name, _.setGroup(group)) }
  def a_project(name: String, project: String) { env = env.transformE(name, _.setProject(project)) }
  def a_heads_group(name: String, group: String) { env = env.transformE(name, _.setGroupHead(true).setGroup(group)) }
  def a_heads_project(name: String, project: String) { env = env.transformE(name, _.setProjectHead(true).setProject(project)) }
  def a_works_with(a: String, b: String) {
    env = env.
      transformE(a, _.addWorksWith(b)).
      transformE(b, _.addWorksWith(a))
  }
  def a_works_with(name: String, others: Set[Pair[ParamType, Object]]) {
    env = others.asScala.
      map(p => p.getValue().asInstanceOf[String]).
      foldRight(env) { (b: String, enviroment: Env) =>
        enviroment.
          transformE(name, _.addWorksWith(b)).
          transformE(b, _.addWorksWith(name))
    }
  }
  def a_assigned_to(name: String, room: String) { 
    val r = env.findOrCreateRoom(room).addOccupant(name)
    if(r.isLeft) {
      throw new Exception(r.left.get)
    } else {
      env = env.put(r.right.get)
    }
    env = env.transformE(name, _.setRoom(room))
  }

  def a_large_project(project: String) { env = env.transformP(project, _.setSize(Large)) }

  def a_close(room: String, other: String) {
    env = env.
      transformR(room, _.addCloseTo(other)).
      transformR(other, _.addCloseTo(room))
  }
  def a_close(room: String, others: Set[Pair[ParamType, Object]]) {
    env = others.asScala.
      map(p => p.getValue().asInstanceOf[String]).
      foldRight(env) { (b: String, enviroment: Env) =>
        enviroment.
          transformR(room, _.addCloseTo(b)).
          transformR(b, _.addCloseTo(room))
    }
  }
  def a_small_room(room: String) { env = env.transformR(room, _.setSize(Small)) }
  def a_medium_room(room: String) { env = env.transformR(room, _.setSize(Medium)) }
  def a_large_room(room: String) { env = env.transformR(room, _.setSize(Large)) }

  def a_in_project(name: String, project: String) { a_project(name, project) }
  def a_in_group(name: String, group: String) { a_group(name, group) }
  def a_assign_to(name: String, room: String) { a_assigned_to(name, room) }

  def e_person(name: String): Boolean = { env.testE(name, x => true) }
  def e_secretary(name: String): Boolean = { env.testE(name, _.isSecretary) }
  def e_researcher(name: String): Boolean = { env.testE(name, _.isResearcher)}
  def e_manager(name: String): Boolean = { env.testE(name, _.isManager) }
  def e_smoker(name: String): Boolean = { env.testE(name, _.isSmoker) }
  def e_hacker(name: String): Boolean = { env.testE(name, _.isHacker) }
  def e_in_group(name: String, group: String): Boolean = { env.testE(name, _.group == group) }
  def e_group(name: String, group: String): Boolean = { e_in_group(name,group) }
  def e_in_project(name: String, project: String): Boolean = { env.testE(name, _.project == project) }
  def e_project(name: String, project: String): Boolean = { e_in_project(name,project) }
  def e_heads_group(name: String, group: String): Boolean = { (e_group(name,group) || e_in_group(name,group)) && env.testE(name, _.isGroupHead) } 
  def e_heads_project(name: String, project: String): Boolean = { (e_project(name,project) || e_in_group(name,project)) && env.testE(name, _.isProjectHead) } 
  def e_works_with(name: String, set: Set[Pair[ParamType, Object]]): Boolean = { set.asScala.foldRight(true)((a, b) => e_works_with(name, a.getValue().asInstanceOf[String]) && b) }
  def e_works_with(name: String, otherEmployee: String): Boolean = { env.testE(name, _.worksWith.contains(otherEmployee)) }
  def e_assign_to(name: String, room: String): Boolean = { false }
  def e_room(room: String): Boolean = { env.testR(room, x => true) }
  def e_close(room: String, otherRoom: String): Boolean = { env.testR(room, _.closeTo.contains(otherRoom)) }
  def e_close(room: String, set: Set[Pair[ParamType, Object]]): Boolean = { set.asScala.foldRight(true)((a, b) => e_close(room, a.getValue().asInstanceOf[String]) && b) }
  def e_large_room(room: String): Boolean = { env.testR(room, x => x.size equals Large) }
  def e_medium_room(room: String): Boolean = { env.testR(room, x => x.size equals Medium) }
  def e_small_room(room: String): Boolean = { env.testR(room, x => x.size == Small) }
  def e_group(group: String): Boolean = { env.groups.contains(group) }
  def e_project(name: String): Boolean = { env.testP(name, x => true)}
  def e_large_project(project: String): Boolean = { env.testP(project, x => x.size equals Large)}

  override def toString() = env.toString()
  
  
  
}
