package cpsc433.benchmark
import scala.collection.immutable.Stack
import java.util.concurrent.atomic.AtomicReference

object BenchmarkService {

  var stack: Map[String, Stack[Bench]] = Map()
  var metas: Map[String, Meta] = Map()
  var start: Long = -1
  
  
  
  def start(sectionName: String) {
      val msec = System.currentTimeMillis()
      if(start < 0) {
        start = msec 
      }
	  val gnu = stack.get(sectionName).getOrElse(Stack()).push(Bench(msec))
	  stack = stack + (sectionName -> gnu)
	  //println("pushed ["+sectionName+"] with result: " + stack(sectionName).toList.map(_.toString))
  }
  
  def end(sectionName: String) {
    
	  val msec = System.currentTimeMillis()
	  //println("poping from ["+sectionName+"]: " + stack(sectionName).toList.map(_.toString))
	  val (top, gnu) = stack(sectionName).pop2
	  stack = stack + (sectionName -> gnu)
	  
	  val meta = metas.get(sectionName).getOrElse(Meta(0, 0))
	  
	  metas = metas + (sectionName -> meta.include(msec - top.msec))
  }
  
   
  
  def currentResults(): String = {
    val cstacks = stack
    val cmetas = metas
    
    val ms = System.currentTimeMillis()
    
    cstacks.toList.sortBy(_._1).map { (pair) => pair match{
      case (name, s) =>
        val finalMeta = s.foldRight(cmetas.get(name).getOrElse(Meta(0,0))){
          (b, m) =>
            m.include(ms - b.msec)
        }
        val measured = ms - start
        new StringBuilder().
          append(name).
          append("\n\t").append("calls:              ").append(finalMeta.calls).
          append("\n\t").append("mSec:               ").append(finalMeta.msecTotal).
          append("\n\t").append("avg time:           ").
            append(finalMeta.msecTotal.toDouble / finalMeta.calls.toDouble).
          append("\n\t").append("% of measured time: ").
            append(finalMeta.msecTotal.toDouble / measured.toDouble * 100).append("%").
          toString
    }}.mkString("\n")
  }
  
}

case class Bench(msec: Long)
case class Meta(msecTotal: Long, calls: Long) {
  def include(msec: Long): Meta = {
    Meta(msecTotal + msec, calls + 1)
  }
}



