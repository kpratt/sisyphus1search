package cpsc433.solution


import org.specs2.mutable._
import java.io.BufferedReader
import java.io.StringReader
import cpsc433.ISolution
import cpsc433.benchmark.BenchmarkService
import cpsc433.ProblemGenerator

class SearchTest extends Specification {
  
      //(int numEmps, int numRooms, int numGroups, int numProjects, int maxClose, int workswith)
  def createProblem(numEmps: Int, numRooms :Int, numGroups : Int , 
      numProjects: Int, maxClose :Int , maxWorksWith :Int){
      val problem = new ProblemGenerator(numEmps, numRooms, numGroups, numProjects, maxClose, maxWorksWith)
      problem.GenerateProblem()
  }
  

  "Search control" should { // faulty compile error
    "get a good result" in { // faulty compile error
      val e = new Environment()
      e.fromFile("./src/test/resource/SampleInput.txt")
      
      val size = Genetics.genomeSize(e.env)
      println("=========================================================")
      println("Genome size:" + size)
      val pool = new GenePool(Genetics.generateN(50, size), 100, e.env)
      pool.simpleSearch(System.currentTimeMillis() + 30000)
      println(BenchmarkService.currentResults())
      println("=========================================================")
    }
  }
  
  "get a good result with file generator" in {
    //ProblemGenerator(int numEmps, int numRooms, int numGroups, int numProjects,  int maxClose, int maxworkswith)
	  val problem = createProblem(40,25,6,10,4,5)
	  val e = new Environment();
	  
	  e.fromFile("out.txt")
	  val size = Genetics.genomeSize(e.env)
      println("=========================================================")
      println("Genome size:" + size)
      val pool = new GenePool(Genetics.generateN(50, size), 100, e.env)
      pool.simpleSearch(System.currentTimeMillis() + 30000)
      println(BenchmarkService.currentResults())
      println("=========================================================")
	  
  }
}