package cpsc433.solution

import org.specs2.mutable._
import java.io.BufferedReader
import java.io.StringReader
import cpsc433.ISolution

class SoftConstraintPenaltyTest extends Specification {

  "Solutions" should {
    "group heads should be close to all members of the group" in {
      //(2) -2 penalty for each group member not close to the head
      val environment = new Environment()
      val stream = createStream("""
		assigned-to(bob, A)
        assigned-to(alex, B)
        assigned-to(john, C)
        assigned-to(sara, D)
          
        close(A, B)
        
        heads-group(bob, group)
        
        group(bob, group)
        group(alex, group)
        group(john, group)
        group(sara, group)
        """)
      environment.fromStream(stream)
      val solution: Solution = new Solution(environment.env)

      solution.peopleCloseTo("A").size must be equalTo (2)
      solution.penalizeGroupMembersFarFromGroupHead must be equalTo (2, -2)
    }

    "managers should be close to their group's head" in {
      //(6) -20 penalty for each not close
      val environment = new Environment()
      val stream = createStream("""
		heads-group(bob, group)
        manager(alex)
        manager(sara)
          
        assigned-to(bob, A)
        assigned-to(alex, B)
        assigned-to(sara, C)
          
        close(A, B)
        
        group(bob, group)
        group(alex, group)
        group(sara, group)
        """)
      environment.fromStream(stream)
      val solution: Solution = new Solution(environment.env)

      solution.penalizeManagersFarFromGroupHead must be equalTo (1, -20) 
    }

    "managers should be close to all members of their group" in {
      //(7) -2 penalty per each not close
      val environment = new Environment()
      val stream = createStream("""
		manager(alex)
        manager(sara)
          
        assigned-to(bob, A)
        assigned-to(alex, B)
        assigned-to(sara, C)
        assigned-to(john, D)
          
        close(A, B)
        close(C, D)
        
        group(bob, group)
        group(alex, group)
        group(sara, group)
        group(john, group)
        """)
      environment.fromStream(stream)
      val solution: Solution = new Solution(environment.env)

      solution.penalizeManagersFarFromGroupMembers must be equalTo (4, -2)
    }

    "the heads of projects should be close to all members of their project" in {
      //(8) -5 penalty per each not close
      val environment = new Environment()
      val stream = createStream("""
		
        heads-project(bob, proj)
          
        assigned-to(bob, A)
        assigned-to(alex, B)
        assigned-to(sara, C)
        assigned-to(john, D)
          
        close(A, B)
        
        project(bob, proj)
        project(alex, proj)
        project(sara, proj)
        project(john, proj)
        """)
      environment.fromStream(stream)
      val solution: Solution = new Solution(environment.env)

      solution.penalizeMembersFarFromThierProjectHeads must be equalTo (2, -5)
    }

    "the heads of large projects should be close to the head of their group" in {
      //(10) -10 penalty per each not close
      val environment = new Environment()
      val stream = createStream("""
		
        large-project(proj)
        heads-project(sara, proj)
        heads-group(bob, proj)
          
        assigned-to(bob, A)
        assigned-to(sara, C)
          
        project(bob, proj)
        project(sara, proj)
        """)
      environment.fromStream(stream)
      val solution: Solution = new Solution(environment.env)

      solution.penalizeGroupHeadsFarFromLargeProjectHeads must be equalTo (1, -10)
    }
    
    "All group heads should have large rooms" in {
      //(10) -10 penalty per each not close
      val environment = new Environment()
      val stream = createStream("""
		
        heads-group(bob, proj)
        heads-group(alice, proj2)  
        small-room(A)
        assigned-to(bob, A)
    	assigned-to(alice,B)
        large-room(B)
        """)
      environment.fromStream(stream)
      val solution: Solution = new Solution(environment.env)

      solution.penalizeSmallOfficeGH must be equalTo (1, -40)
    }
    
    "All group heads should have a secretary of their group close by" in {
      //(10) -10 penalty per each not close
      val environment = new Environment()
      val stream = createStream("""
		
        heads-group(phil, a)
        group(phil, a)
        secretary(jan)
        group(jan, a)
          
        heads-group(may,b)
        group(may, b)
        secretary(june)
        group(june, b)
          
        heads-group(phile,ab)
        group(phile, ab)
        secretary(jane)
        group(jane, ab)
          
        close(A, B)
        close(D, E)
          
        assigned-to(phil, A)
        assigned-to(jan, B)
         assigned-to(june, B)
        assigned-to(may, C)
         assigned-to(jane, D)
        assigned-to(phile, E)
          
        project(bob, proj)
        project(sara, proj)
        """)
      environment.fromStream(stream)
      val solution: Solution = new Solution(environment.env)

      solution.penalizeSecretaryFarGH must be equalTo (1, -30)
    }
    
    "All project heads (large projects) should have a secretary of their group close by" in {
      //(10) -10 penalty per each not close
      val environment = new Environment()
      val stream = createStream("""
		
        heads-project(phil,a)
        group(phil, a)
        secretary(jan)
        group(jan, a)
          
        heads-project(may,b)
        group(may, b)
        secretary(june)
        group(june, b)
          
        heads-project(phile,ab)
        group(phile, ab)
        secretary(jane)
        group(jane, ab)
          
        close(A, B)
        close(D, E)
          
        assigned-to(phil, A)
        assigned-to(jan, B)
        assigned-to(june, B)
        assigned-to(may, C)
        assigned-to(jane, D)
        assigned-to(phile, E)
          
        project(bob, proj)
        project(sara, proj)
        project(may, b)  
        large-project(b)
        """)
      environment.fromStream(stream)
      val solution: Solution = new Solution(environment.env)

      solution.penalizeSecretaryFarPH must be equalTo (1, -10)
    }
    
    "All managers should have a secretary of their group close by" in {
      //(10) -10 penalty per each not close
      val environment = new Environment()
      val stream = createStream("""
		
        manager(phil)
        group(phil, a)
        secretary(jan)
        group(jan, a)
          
        manager(may)
        group(may, b)
        secretary(june)
        group(june, b)
          
        manager(phile)
        group(phile, ab)
        secretary(jane)
        group(jane, ab)
          
        close(A, B)
        close(D, E)
          
        assigned-to(phil, A)
        assigned-to(jan, B)
         assigned-to(june, B)
        assigned-to(may, C)
         assigned-to(jane, D)
        assigned-to(phile, E)
          
        project(bob, proj)
        project(sara, proj)
        """)
      environment.fromStream(stream)
      val solution: Solution = new Solution(environment.env)

      solution.penalizeSecretaryFarM must be equalTo (1, -20)
    }
    
    "secretaries should share rooms" in {
      //(10) -10 penalty per each not close
      val environment = new Environment()
      val stream = createStream("""
		
        secretary(bob)
        secretary(sara)
        person(a)
        person(b)
          
        assigned-to(bob, A)
          assigned-to(a, A)
        assigned-to(b, B)
          assigned-to(sara, B)
          
        project(bob, proj)
        project(sara, proj)
        """)
      environment.fromStream(stream)
      val solution: Solution = new Solution(environment.env)

      solution.penalizeSecretaryNonShare must be equalTo (2, -5)
    }
    
    "a smoker and a non-smoker should not share a room" in {
      //(10) -10 penalty per each not close
      val environment = new Environment()
      val stream = createStream("""
		
        hacker(sara)
        smoker(bob)
          
        assigned-to(bob, A)
        assigned-to(sara, A)
          
        project(bob, proj)
        project(sara, proj)
        """)
      environment.fromStream(stream)
      val solution: Solution = new Solution(environment.env)

      solution.penalizeSmokerWithNon must be equalTo (1, -50)
    }
  }
  
  
  def createStream(str: String): BufferedReader = {
    new BufferedReader(new StringReader(str));
  }
  
}