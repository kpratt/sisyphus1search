package cpsc433.solution

import org.specs2.mutable._
import java.io.BufferedReader
import java.io.StringReader
import cpsc433.ISolution

class EnvTest extends Specification {

  "Data helpers" should { // faulty compile error
    "check employee status" in { // faulty compile error
      var env = EnvData().transformE("Bob", _.setResearcher(true))
      
      env.employees must have size(1)
      env.employees.get("Bob") must not beEmpty
      
      if(! env.testE("Bob", _.isResearcher))
        failure("WTG?")
      else
        success
    }
    
    
    "Members of the same project should not share offices" in {
      val environment = new Environment()
      val stream = createStream("""
		assigned-to(bob,A)
        assigned-to(alice,A)
        project(bob, proj)
        project(alice,proj)
        """)
      environment.fromStream(stream)
      val sol: Solution = new Solution(environment.env)
      sol.penalizeMembersOfSameProjectSharingOffice must be equalTo (2,-7)
    }
    
    "check constraint 13" in { //TODO what is constraint 13?
      // under what condidtions does this not apply? something about secrataries?
      val environment = new Environment()
      val stream = createStream("""
		assigned-to(bob,A)
        assigned-to(alice,A)
        hacker(bob)
        project(bob, proj)
        project(alice,proj)
        """)
      environment.fromStream(stream)
      val sol: Solution = new Solution(environment.env)
      sol.penalizeHackersSharingOfficesWithNonHackers must be equalTo (2,-2)
    }
    
    "People should not share offices" in {
      
      val environment = new Environment()
      val stream = createStream("""
		assigned-to(bob, A)
        assigned-to(alice, A)
        room(A)
        """)
      environment.fromStream(stream)
      val solution: Solution = new Solution(environment.env)
      solution.penalizePeopleSharingOffices must be equalTo (2,-4)
      
    }
    
    "People sharing offices should work together" in {
    val environment = new Environment()
      val stream = createStream("""
		assigned-to(bob, A)
        assigned-to(alice, A)
        works-with(bob , {charles, eve})
        """)
      environment.fromStream(stream)
      val sol: Solution = new Solution(environment.env)   
      sol.penalizePeopleSharingOfficeAndDontWorkTogether must be equalTo (2,-3)
    }
    //
    "People should not share small offices" in {
    
      val environment = new Environment()
      val stream = createStream("""
		assigned-to(bob, A)
        assigned-to(alice, A)
        small-room(A)
        """)
      environment.fromStream(stream)
      val sol: Solution = new Solution(environment.env)
      sol.penalizePeopleSharingSmallRoom must be equalTo (2,-25)
    }
  }
    def createStream(str: String): BufferedReader = {
    new BufferedReader(new StringReader(str));
  }
}