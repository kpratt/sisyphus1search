package cpsc433.solution
import org.specs2.mutable._
import java.io.BufferedReader
import java.io.StringReader
import cpsc433.ISolution
import cpsc433.benchmark.BenchmarkService
import cpsc433.ProblemGenerator

class GeneticsTest extends Specification {

  "Operators" should {
    "mutate Genomes into new values" in {
      val original = Genome(List(6,6,6,6,6))
      val mutant = Genetics.mutate(original)
      val differences = original.selectors.zip(mutant.selectors).map(pair => if(pair._1 == pair._2) 0 else 1).foldRight(0)(_+_)
      differences must be equalTo(1)
    }
    
    "mutate Genomes into radically new values" in {
      val original = Genome(List(6,6,6,6,6))
      val mutant = Genetics.sequenceN(Genetics.mutate)(3)(original)
      val differences = original.selectors.zip(mutant.selectors).map(pair => if(pair._1 == pair._2) 0 else 1).foldRight(0)(_+_)
      println("o:" + original)
      println("m:" + mutant)
      differences must beLessThanOrEqualTo(3)
      differences must beGreaterThanOrEqualTo(1)
    }
  }

}
