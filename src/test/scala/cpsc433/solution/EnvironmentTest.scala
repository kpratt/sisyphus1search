package cpsc433.solution

import org.specs2.mutable._
import java.io.BufferedReader
import java.io.StringReader

class EnvironmentTest extends Specification {

  "The sample problem should be solvable" should { // faulty compile error

    "Duplication employee" in { // faulty compile error
      val environment = new Environment()
      val stream = createStream("""
		person(bob)
		person(bob)
		""")
      environment.fromStream(stream)

      //TODO Real assertions
      true must beTrue

    }

    "Duplication project" in { // faulty compile error
      val environment = new Environment()
      val stream = createStream("""
		project(Y)
		project(Y)
		""")
      environment.fromStream(stream)

      //TODO Real assertions
      true must beTrue

    }

    "Duplication group" in { // faulty compile error
      val environment = new Environment()
      val stream = createStream("""
		group(X)
		group(X)
		""")
      environment.fromStream(stream)

      //TODO Real assertions
      true must beTrue

    }

    "Reflexive" in { // faulty compile error
      val environment = new Environment()
      val stream = createStream("""
person(tim)
person(james)
works-with(james, {tim})
""")
      environment.fromStream(stream)

      environment.e_person("tim") must beTrue
      environment.e_person("james") must beTrue
      environment.e_works_with("tim", "james") must beTrue
      environment.e_works_with("james", "tim") must beTrue
    }

    "Lets read in the sample problem" in {
      val e = new Environment()
      e.fromFile("./src/test/resource/SampleInput.txt")

      val e2 = new Environment()
      e.fromStream(createStream(e.toString))

      e2 must be equalTo (e)
    }

    "Lets find a researcher" in {
      val environment = new Environment()
      val researcher = "Werner"
      val stream = createStream("""
		researcher(Bob)
		project(Bob, YY)
		researcher(Alice)
		group(Alice, Some Group)
		""")
      environment.fromStream(stream)
      (environment.e_person(researcher) must beFalse) &&
        (environment.e_researcher(researcher) must beFalse)
    }

    "Lets find a project head" in {
      val environment = new Environment()
      val projecthead = "Bob"
      val stream = createStream("""
		researcher(Bob)
		project(Bob, YY)
		researcher(Alice)
		group(Alice, Some Group)
		""")
      environment.e_person(projecthead) must beFalse
    }

    "Lets find if a person belongs to a group" in
      {
        val environment = new Environment()
        var group = "Some Group"
        var person = "Alice"
        val stream = createStream("""
			researcher(Bob)
			project(Bob, YY)
			researcher(""" + EnvData.quote(person) + """)
			group(Alice, """ + EnvData.quote(group) + """)
			""")
        environment.fromStream(stream)

        (environment.e_person("Bob") must beTrue) &&
          (environment.e_group(group) must beTrue) &&
          (environment.e_in_group(person, group) must beTrue)
      }

    "Lets find if a room is close to another room" in {
      val environment = new Environment()
      var room = "R10"
      var otherroom = "R312"
      var stream = createStream("""
			small-room (R10)
			small-room (R312)
			close(R10, R312)
			""")
      environment.fromStream(stream)

      (environment.e_room(room) must beTrue) &&
        (environment.e_room(otherroom) must beTrue) &&
        (environment.e_close(room, otherroom) must beTrue) &&
        (environment.e_close(otherroom, room) must beTrue)
    }

    "Lets find if a person can belong to two groups" in {
      val environment = new Environment()
      val group1 = "group1"
      val group2 = "group2"
      val person = "Alice"
      val stream = createStream("""
		researcher(Alice)
		group(Alice, group1)
        group(Alice, group2)
		""")
      environment.fromStream(stream)
      // should be assigned to the last group specified in the file according to the FAQ page
      (environment.e_in_group("Alice", group1) must beFalse) &&
        (environment.e_in_group("Alice", group2) must beTrue)
    }

    "Lets find if a room can have two sizes" in {

      val environment = new Environment()
      val stream = createStream("""
		researcher(Alice)
		small-room(AB)
        large-room(AB)
		""")
      environment.fromStream(stream)

      (environment.e_small_room("AB") must beFalse) &&
        (environment.e_large_room("AB") must beTrue)
    }
    
  }

  def createStream(str: String): BufferedReader = {
    new BufferedReader(new StringReader(str));
  }

}
