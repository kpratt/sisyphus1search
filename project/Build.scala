import sbt._
import Keys._

object BuildSettings {
  val buildOrganization = "dualitystudios"
  val buildVersion = "0.1"
  val buildScalaVersion = "2.10.0-RC2"

  val buildSettings = Defaults.defaultSettings ++ Seq(
    organization := buildOrganization,
    version := buildVersion,
    scalaVersion := buildScalaVersion)
}

object Dependencies {
  val scalaz =         "org.scalaz" % "scalaz-core_2.10.0-RC2" % "7.0.0-M4"
  val specs2 =         "org.specs2" % "specs2_2.10.0-RC2" % "1.13-SNAPSHOT" % "test"
}

object Resolvers {
  //val scala_testing = "Scala Testing" at "http://mvnrepository.com/artifact"
  lazy val JavaNet = "Java.net Maven2 Repository" at "http://download.java.net/maven/2/"
  lazy val scalaTools = "Scala-Tools Snapshots" at "http://scala-tools.org/repo-snapshots/"
  lazy val sona_snap = "snapshots" at "http://oss.sonatype.org/content/repositories/snapshots"
  lazy val sona_release = "releases"  at "http://oss.sonatype.org/content/repositories/releases"
  def resolve_all = Seq(JavaNet, scalaTools, sona_snap, sona_release)
}

object LiftProject extends Build {

  import Dependencies._;
  import BuildSettings._;
  import Resolvers._;

  lazy val main = Project(
    "Sisyphus1Search",
    file("."),
    settings = buildSettings ++ Seq(
      resolvers := resolve_all,
      libraryDependencies ++= Seq(
        specs2,
        scalaz
      ),
      parallelExecution in Test := false
    )
    
      
    )

}
